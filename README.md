CharCheck - Plain Text File Character Set Checking Tool
=======================================================

Simple console tool for checking plain text file specified character set conformity.

SYSTEM REQUIREMENTS
-------------------

Installed Oracle Java Runtime Environment - version at least 1.7
https://java.com/download/

SUPPORTED CHARACTER SETS
------------------------

* ISO 8859-2 https://en.wikipedia.org/wiki/ISO/IEC_8859-2
* ASCII

USAGE
-----

To check file file_to_check.txt to be ISO-8859-2 compatible:

java -jar CharCheck.jar file_to_check.txt

Note that now only ISO-8859-2 charset check is supported.


LICENSE
-------
Licensed under the [GPL](http://www.gnu.org/licenses/gpl.html).
