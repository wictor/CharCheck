package cz.sysrq.charcheck;

import java.util.HashSet;
import java.util.Set;

/**
 * TODO VSV: JavaDoc.
 *
 * @author vsvoboda
 * @since 10.12.2016
 */
public abstract class AbstractCharsetValidator implements CharsetValidator {

    @Override
    public CheckResult checkText(byte[] buffer, int len) {
        CheckResult result = new CheckResult();
        for (int i = 0; i < len; i++) {
            byte c = buffer[i];

            if (isValidChar(c & 0xff)) {
                if (isNonAsciiChar(c & 0xff)) {
                    result.getNonAsciiBytes().add(c);
                }
                // it is plain old ASCII text character byte - OK
            } else {
                result.getInvalidBytes().add(c);
            }
        }
        return result;
    }

    @Override
    public CheckResult checkText(byte[] buffer) {
        return checkText(buffer, buffer.length);
    }
}
