package cz.sysrq.charcheck;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Set;

/**
 * TODO VSV: JavaDoc.
 *
 * @author vsvoboda
 * @since 10.12.2016
 */
public class FileCheck {

    private final Logger logger = LoggerFactory.getLogger(FileCheck.class);

    private final File file;


    public FileCheck(File file) {
        this.file = file;
    }

    public FileCheck(Path filePath) {
        this.file = filePath.toFile();
    }

    public CheckResult check(CharIso88592 charCheck) {
        CheckResult result = new CheckResult();
        try (InputStream ios = new FileInputStream(file)) {
            logger.info("Reading file: {}", file);
            byte[] buffer = new byte[4096];
            int fileRead = 0;
            int read;
            while ((read = ios.read(buffer)) != -1) {
                logger.debug(".. reading {} bytes", read);
                result.addAll(charCheck.checkText(buffer, read));
                fileRead += read;
            }

            logger.debug("File read OK: bytes read {}", fileRead);
            logger.debug("Result: {}", result);
        } catch (FileNotFoundException e) {
            logger.error("File not found: {}", e.getMessage());
        } catch (IOException e) {
            logger.error("IO Error", e);
        }
        return result;
    }

}
