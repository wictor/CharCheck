package cz.sysrq.charcheck;

import java.util.Set;

/**
 * TODO VSV: JavaDoc.
 *
 * @author vsvoboda
 * @since 10.12.2016
 */
public interface CharsetValidator {

    boolean isValidChar(int c);

    boolean isNonAsciiChar(int c);

    CheckResult checkText(byte[] buffer, int len);

    CheckResult checkText(byte[] buffer);
}
