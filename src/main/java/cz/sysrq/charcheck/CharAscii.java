package cz.sysrq.charcheck;

/**
 * TODO VSV: JavaDoc.
 *
 * @author vsvoboda
 * @since 10.12.2016
 */
public class CharAscii extends AbstractCharsetValidator {

    private static final int ASCII_TAB = 0x09;
    private static final int ASCII_LF = 0x0A;
    private static final int ASCII_CR = 0x0D;

    @Override
    public boolean isValidChar(int c) {
        return !isNonAsciiChar(c);
    }

    @Override
    public final boolean isNonAsciiChar(int c) {
        return (c < 0x20 || c > 0x7e) && c != ASCII_TAB && c != ASCII_LF && c != ASCII_CR;
    }

}
