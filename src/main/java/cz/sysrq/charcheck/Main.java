package cz.sysrq.charcheck;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Charset checker main class.
 *
 * @author vsvoboda
 * @since 10.12.2016
 */
public class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        logger.info("CharCheck hello");

        if (args.length > 0) {
            Path filePath = Paths.get(args[0]);
            FileCheck fileCheck = new FileCheck(filePath);
            CheckResult result = fileCheck.check(new CharIso88592());

            logger.info("> {}", result);

            if (!result.isEmpty()) {
                createSpecExtract(filePath, result);
                createDescExtract(filePath, result);
            }
        } else {
            logger.warn("No file passed to check");
        }



        logger.info("CharCheck bye");

    }

    // SPEC file - contains only non-ASCII characters

    private static void createSpecExtract(Path filePath, CheckResult result) {
        Path specFile = specFilePath(filePath);
        logger.info("Writing special characters file {}", specFile);

        byte[] buffer = new byte[result.getNonAsciiBytes().size() + result.getInvalidBytes().size()];
        int index = 0;

        for (byte c : result.getNonAsciiBytes()) {
            buffer[index++] = c;
        }

        for (byte c : result.getInvalidBytes()) {
            buffer[index++] = c;
        }

        logger.debug("Going to write {} bytes", index);

        try {
            Files.write(specFile, buffer);
        } catch (IOException e) {
            logger.error("Writing special characters file failed", e);
        }
    }

    static Path specFilePath(Path origFile) {
        String newPathString = String.format("%s.spec", origFile);
        return Paths.get(newPathString);
    }

    // DESC file - contains only non-ASCII characters + hex dump

    private static void createDescExtract(Path filePath, CheckResult result) {
        Path specFile = descFilePath(filePath);
        logger.info("Writing special characters description file {}", specFile);

        String nonAsciiBytesHexDump = result.getNonAsciiBytesHexDump();
        String invalidBytesHexDump = result.getInvalidBytesHexDump();
        byte[] nonAsciiBytes = nonAsciiBytesHexDump.getBytes();
        byte[] invalidBytes = invalidBytesHexDump.getBytes();

        int specByteLen = result.getNonAsciiBytes().size() + result.getInvalidBytes().size();
        byte[] buffer = new byte[3 * specByteLen];
        int index = 0;

        byte SPACE = 0x20;

        for (byte c : result.getNonAsciiBytes()) {
            buffer[index++] = SPACE;
            buffer[index++] = SPACE;
            buffer[index++] = c;
        }

        for (byte c : result.getInvalidBytes()) {
            buffer[index++] = SPACE;
            buffer[index++] = SPACE;
            buffer[index++] = c;
        }

        index = 0;
        byte[] buffer0 = new byte[buffer.length + 2 + nonAsciiBytes.length + invalidBytes.length + 2];
        System.arraycopy(buffer, 0, buffer0, index, buffer.length);
        index += buffer.length;
        buffer0[index++] = 0x0d;
        buffer0[index++] = 0x0a;
        System.arraycopy(nonAsciiBytes, 0, buffer0, index, nonAsciiBytes.length);
        index += nonAsciiBytes.length;
        System.arraycopy(invalidBytes, 0, buffer0, index, invalidBytes.length);
        index += invalidBytes.length;
        buffer0[index++] = 0x0d;
        buffer0[index++] = 0x0a;

        logger.debug("Going to write {} bytes", index);

        try {
            Files.write(specFile, buffer0);
        } catch (IOException e) {
            logger.error("Writing special characters file failed", e);
        }
    }

    static Path descFilePath(Path origFile) {
        String newPathString = String.format("%s.desc", origFile);
        return Paths.get(newPathString);
    }
}
