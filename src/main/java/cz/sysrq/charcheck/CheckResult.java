package cz.sysrq.charcheck;

import java.util.HashSet;
import java.util.Set;

/**
 * TODO VSV: JavaDoc.
 *
 * @author vsvoboda
 * @since 10.12.2016
 */
public class CheckResult {

    private final Set<Byte> nonAsciiBytes;
    private final Set<Byte> invalidBytes;

    public CheckResult(Set<Byte> nonAsciiBytes, Set<Byte> invalidBytes) {
        this.nonAsciiBytes = nonAsciiBytes;
        this.invalidBytes = invalidBytes;
    }

    public CheckResult() {
        this(new HashSet<Byte>(), new HashSet<Byte>());
    }

    public void addAll(CheckResult subResult) {
        nonAsciiBytes.addAll(subResult.getNonAsciiBytes());
        invalidBytes.addAll(subResult.getInvalidBytes());
    }

    public boolean isEmpty() {
        return nonAsciiBytes.isEmpty() && invalidBytes.isEmpty();
    }

    public Set<Byte> getNonAsciiBytes() {
        return nonAsciiBytes;
    }

    public Set<Byte> getInvalidBytes() {
        return invalidBytes;
    }

    public String getNonAsciiBytesHexDump() {
        return bytesToHexString(nonAsciiBytes);
    }

    public String getInvalidBytesHexDump() {
        return bytesToHexString(invalidBytes);
    }

    @Override
    public String toString() {
        String nonAscii = nonAsciiBytes != null && !nonAsciiBytes.isEmpty() ? bytesToHexString(nonAsciiBytes) : "not found";
        String invalid = invalidBytes != null && !invalidBytes.isEmpty() ? bytesToHexString(invalidBytes): "not found";

        return String.format("CheckResult: Non-ASCII bytes: %s. Invalid bytes: %s.", nonAscii, invalid);
    }

    private static String bytesToHexString(Set<Byte> bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format(" %02X", b));
        }
        return sb.toString();
    }
}
