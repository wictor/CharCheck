package cz.sysrq.charcheck;

/**
 * TODO VSV: JavaDoc.
 *
 * @author vsvoboda
 * @since 10.12.2016
 */
public class CharIso88592 extends CharAscii {

    public boolean isValidChar(int c) {
        return super.isValidChar(c) || c >= 0xa0;
    }

}
