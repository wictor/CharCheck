package cz.sysrq.charcheck;

import org.junit.Test;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.*;

/**
 * TODO VSV: JavaDoc.
 *
 * @author vsvoboda
 * @since 11.12.2016
 */
public class MainTest {

    @Test
    public void testSpecFilePath() {
        File file = new File("src/test/resources/file-iso8859-2.txt");
        assertTrue(file.exists());

        //Path path = file.toPath();
        Path path = Paths.get("file-iso8859-2.txt");

        System.out.println("Name = " + path.getFileName());
        System.out.println("Path = " + path.getParent());

        Path specFilePath = Main.specFilePath(path);

        System.out.println("SpecPath = " + specFilePath);

        assertEquals("file-iso8859-2.txt.spec", specFilePath.getFileName().toString());
    }

}