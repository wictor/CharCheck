package cz.sysrq.charcheck;

import org.junit.Test;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.*;

/**
 * TODO VSV: JavaDoc.
 *
 * @author vsvoboda
 * @since 10.12.2016
 */
public class FileCheckTest {

    @Test
    public void testCheckPass() {
        File file = new File("src/test/resources/file-iso8859-2.txt");
        assertTrue(file.exists());

        FileCheck fileCheck = new FileCheck(file);
        CheckResult result = fileCheck.check(new CharIso88592());

        assertNotNull(result);
        assertEquals(0, result.getInvalidBytes().size());
        assertEquals(16, result.getNonAsciiBytes().size());
    }

    @Test
    public void testCheckFailIsoOnUtf() {
        // checking UTF-8 file - so there should be some ISO-8859-2 invalid bytes
        File file = new File("src/test/resources/file-utf8.txt");
        assertTrue(file.exists());

        FileCheck fileCheck = new FileCheck(file);
        CheckResult result = fileCheck.check(new CharIso88592());

        assertNotNull(result);
        assertEquals(5, result.getInvalidBytes().size());
        assertEquals(10, result.getNonAsciiBytes().size());
    }

    @Test
    public void testCheckFailIsoOnWin() {
        // checking win1250 file - so there should be some ISO-8859-2 invalid bytes
        File file = new File("src/test/resources/file-win1250.txt");
        assertTrue(file.exists());

        FileCheck fileCheck = new FileCheck(file);
        CheckResult result = fileCheck.check(new CharIso88592());

        assertNotNull(result);
        assertEquals(3, result.getInvalidBytes().size());
        assertEquals(9, result.getNonAsciiBytes().size());
    }

}