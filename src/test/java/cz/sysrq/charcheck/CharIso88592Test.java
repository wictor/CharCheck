package cz.sysrq.charcheck;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * TODO VSV: JavaDoc.
 *
 * @author vsvoboda
 * @since 10.12.2016
 */
public class CharIso88592Test {

    private final CharIso88592 charIso88592 = new CharIso88592();

    @Test
    public void testIsValidChar() {
        assertTrue(charIso88592.isValidChar(0x20)); // SPACE
        assertTrue(charIso88592.isValidChar(0x61)); // a
        assertTrue(charIso88592.isValidChar(0xf2)); // n+
        assertTrue(charIso88592.isValidChar(0xA0)); // NBSP

        assertTrue(charIso88592.isValidChar(0x09)); // TAB
        assertTrue(charIso88592.isValidChar(0x0A)); // LF
        assertTrue(charIso88592.isValidChar(0x0D)); // CR

        assertFalse(charIso88592.isValidChar(0x00)); // NULL
        assertFalse(charIso88592.isValidChar(0x07)); // BELL
        assertFalse(charIso88592.isValidChar(0x08)); // Backspace
        assertFalse(charIso88592.isValidChar(0x0b)); // Vertical TAB
        assertFalse(charIso88592.isValidChar(0x1b)); // ESC
        assertFalse(charIso88592.isValidChar(0x1f)); // Unit separator
        assertFalse(charIso88592.isValidChar(0x7f)); // DEL
    }

    @Test
    public void testIsNonAsciiChar() {
        assertTrue(charIso88592.isNonAsciiChar(0xF2));
        assertTrue(charIso88592.isNonAsciiChar(0xB9));

        assertFalse(charIso88592.isNonAsciiChar(0x41));
        assertFalse(charIso88592.isNonAsciiChar(0x20));
    }
}