PM Rev v38 ZR'17
----------------

DEV team:
RMU 1.0
AMO 1.0 / testy
JCO 1.0 / testy
VSV 0.6
LSU 1.0
VLE 1.0
JJE 1.0 / testy
  = 6.6 FTE

TEST team:
MVO 1.0
JPI 1.0
JHE 0.2
SHO 0.6 (od �nora zase asi 0.8-1.0)
VSP 0.5
MLudma
MVachovec
  = 3.3
----------------

Revize 27.9.2016 ----------------------------------------------------

Branch v38 ud�lat v pond�l� a zah�jit v�voj ZR: RMU a LSU
DES na MBA ud�l� LSU s LDA spole�n�, aby se LDA taky nau�il

Nov� TSPI pro zji�t�n� na�ich WS - TJen��ek monitoring
 -> SPI-213 Monitoring MCI WS -> p�i�adit n�komu, kdo na to bude m�t trochu �as - VLE LSU?

Aktu�ln� k v�voji (za�azeno a je hotov� design):
CR-2605 Skr�v�n� reklamy v S24 MB         -> .. CBL + MBA S24
CR-2556 CDM - upravit logiku distribuce HUBu..  - dle DES bez dopadu - pouze testy
CR-2568 Okam�it� ru�en� souhlas� s inkasem      - dle DES bez dopadu - pouze testy
CR-2557 Migrace klientsk�ch n�zv� ��t�    -> .. DB replika
CR-2558 Migrace �ablon mobiln�ch plateb   -> .. DB replika

LSU aktu�ln� ji� d�l� CR-2549.

Revize 18.10.2016 ----------------------------------------------------

PM:
- nena��t� se utilizace -> zt��en� sledov�n� stavu utilizace CR

INFRA:
- branch v38 OK
- st�le se �ek� na pln� funk�n� build v38 na nov�m Jenkinsovi, aby pro�el build lok�ln� -> probl�m prostup� z XUC (csin.cz) do csint.cz

DEV:
- aktu�ln� b�� hlavn� DES (LSU, RMU, AMO, VSV), ale i u� i DEV (LSU, JCO, VSV):
 - CR-2549 MBA SSL cert      - LSU : dev done
 - CR-2548 MBA Obfuskace     - LSU : S24 done, B24 nen� (cca 60%)
 - CR-2550 MBA Upgrade Andr  - LSU : dev done
 - CR-2076 MBA Upgrade iOS   - JCO : prob�h�
 - CR-2538 S24 BESC          - VSV : prob�h�

- LSU u� m� zad�n� pr�ce i od P.B��ka
- LSU dod�l� design CR-2548 + CR-2550
- MBA CR jsou t�mto pokryta - a� na CR-2605 - drobnost, dobr� na zau�ov�n�

lidi:
dev:
 VSV
 AMO
 RMU - CBL
 JJE - CBL/IB/DB
 LSU - MBA, B��k
 JCO - iOS, mobiln� kompetence

 DPE - 0.2 jen? mobiln� kompetence
 SHO - 0.2 jen? mobiln� kompetence
 
 MBA - APCIC/TRACE
 PMI - APCIC/APMBA
 LDA - DES
tst:
 JHE
 MVO
 VSP
 
 JPI
 MPO
 
 DPE - 0.8
 SHO - 0.8
.

** VSP - probrat mo�nosti zlep�en� organizace pr�ce
MBA - �kolen� time management 10.-11.11.2016


my tasks todo:
do�e�it v�vojov� prost�ed� v38 - local build + aktualizace CNFL postupu (SPI-214)
designy
do�e�it revize
aktualizovat seznam TSPI pro ZR - proj�t si i n�co z t�ch star��ch
mobiln� kompetence
Upsource - napojen� na YT (SPI-209)
p�edat n�komu ten SonarQube (SPI-074) - mo�n� p�evz�t k sob� nyn�
DB unit testy (SPI)

Revize 25.10.2016 ----------------------------------------------------

V37 m� j�t na ST1 ve �tvrtek. V38 na ST2 na za��tku p���t�ho t�dne. Uvid�me.
Pomigra�ku chceme skon�it zejtra.


revize DES:
- MBA win SSL - Maximau - reivze designu, resp. �e�en�
- B24 Ulo�en� rozprac

p�rov� program s RMU a VSV
domluvit se Petrem Michali�kou na zau�en� do APCIC
n�kdo p�idat na Trace - SHO?

TSPI
- unit testy
- integra�n� testy
- AUT
- sb�r�n� metrik

TODO
 - merge v37->v38


Revize 2.11.2016 ----------------------------------------------------

ST1 migrov�no na V37
ST2 m� b�t migrov�no na V38 dnes

P���t� t�den IFAT - soust�edit se te� na integra�n� v�ci:
- CR-2538 - OK
- CR-2557, CR-2558 (repliky) - !!! zadat co nejd��ve
- CR-2573 - AMO
- CR-2570 - Trace - d�t SHO
- CR-2605 - bude

Stav Jenkinse:
- build a deploy na AT2, ST2
- merge monitor

P�ipravit si POPO s JCO a AMO.


Aktu�ln� TSPI pro kohokoliv
- SPI-215 Vy�i�t�n� _grants.sql  -- AMO
- SPI-217 Nekonzistence DB schematu  -- AMO
- SPI - unit testy pro DB

Aktu�ln� TSPI pro specialisty
- SPI-206 Zdroj ��seln�k� v Demu  -- RMU
- SPI-074 Hudson findbugs/checkstyle jako sou��st buildu  -- VSP
- SPI-209 Upsource - pilot  -- VSV
- SPI-214 Aktualizace postupu p��pravy v�vojov�ho prost�ed� CIC  -- VSV
- SPI-211 MBA Source Code Policy  -- VSV ve spolupr�ci s LSU
- SPI-212 Trackov�n� zm�n v EA  -- TJE
- SPI-203 Pou�it� lok�ln�ch artefakt� pro build integra�n�ch test�  -- TJE
- SPI-216 Infra monitor web dashboard  -- TSE, VSV

7.11.2016 ------------------------------------------

Revize nacen�n� po�adavk� v PR2016 - z�v�ry:
TODO - doplnil z mailu

Dal�� v�ci:
- bude akutn� CR na n�jak� nov� dokumenty u sjednan�ch �v�rov�ch produkt� (non-plugin) (CR-2688)
  - m� b�t implementov�no do konce t�dne
- �im�nek taky av�zoval nov� CR-2692 DBR1 - Roz���it d�lku pole pro "N�zev" Dobr� rady na 150 znak�
  - mus� se stihnout taky patchem ASAP
do verze:
- CR-2076 MBA Upgrade iOS - bylo vyjedn�no n�jak� nav��en�, je nyn� mo�n� odblokovat dokon�en� v�voje p�i hranici 2 MD do p�ed�n� do test� -> LSU-JCO
- CR-2600_04 Ulo�en� rozpracovan� hromadn� platby - implementovat dle zad�n� -> m��e dostat DPE

V�zva k mor�lce vykazov�n�

to:dl-cic-prof

P��tel�, kamar�di!

mno�� se n�m tu p��pady zanedb�van� utilizace - pozdn�ho vykazov�n� a opravn�ch z�znam�. Tyto situace n�m i v�m pak berou zbyte�n� v�ce �asu, ne� by bylo nutn�.

Cht�l bych t�mto po��dat, abychom se znovu pokusili v�ce p�ibl��it ide�lu vykazov�n� ka�d� den. Ka�d� den po 18:00 b�� automatick� job, kter� kontroluje utilizaci. Je tak mo�n� chyby v utilizaci opravovat rychleji a s men��m rozsahem, pokud vykazujete �ast�ji, ne� kdy� vyk�ete zp�tn� cel� t�den najednou.
Nav�c ka�d� �ter� se synchronizuje pl�n pr�ce a utilizace a kontroluj� se kapacity t�mu. Pokud n�kdo vykazuje pozd�ji a t�eba cel� t�den najednou, doch�z� k pom�rn� velk�mu zkreslen�, co� m��e v�st k chybn�m rozhodnut�m o alokaci lid� na r�zn� �koly a mezi projekty.

V�ichni v�me, �e utilizace nen� v�c, kterou �lov�k d�l� s radost�, ale bez n� nen� mo�n� v�st efektivn� na�e projekty. St�le se sna��me udr�ovat pravidla vykazov�n� stabiln� a co nejjednodu��� p�i zachov�n� pot�ebn�ch v�sledk�. Pokud m�te n�kdo k tomu konkr�tn� p�ipom�nky, nev�hejte se na m� �i Honzu nebo Martina obr�tit.

P�ipom�n�m tak�, �e existuje tabulka s �ablonami typick�ch �kol� na projektu, kter� by v�m m�la vykazov�n� usnadnit a i trochu nav�st.
https://docs.google.com/spreadsheets/d/1NFps_1o9GcQBsq178bBo38JEpcdaUaRNjFZB4COgBWE/edit#gid=1839774015
Tak� pravidla vykazov�n�: https://docs.google.com/spreadsheets/d/1PPfOk8hn6O7INmvMlA4BoXWS0wONk7Et9dGUngWbHS8/edit#gid=0

Pokud si v jak�koliv situaci nejste jist�, kam vyk�zat danou �innost, nev�hejte se zeptat. Ide�ln� rovnou p�i p�ijet� takov�ho �kolu.

D�ky za pozornost.
Va�i TL
.


Stav fun:
*DPE
 -CR-2605 Skr�v�n� reklamy - CBL+Android hotovo
 -CR-2637 Av�za - nev�, dnes na tom za�al - odhad DEV (5h) mu byl sd�len
   - pot�ebuje k vyzkou�en� n�jak� data, asi nebude hned
 -CR-2600_4 Rozprac hromadn� platba
*RMU
 -CR-2557 N�zvy ��t� - hotovo a� na n�zvy replik - �ek�me - �e�� AMO - jinak hotovo
 -SPI-206 Zdroj ��seln�k� - asi to bude cht�t n�jakou brainstormovou sch�zku s MSI
 poslat mu n�jak� revize
*AMO - dnes dovolen�
 -CR-2573 Zm�ny z pilotu - t�m�� hotovo - bude revize, cca 1 MD
 -SPI-215 grants
 -SPI-217 nekonzistence DB
*LSU
 -CR-2600_5 Zobrazen� celkov� ��stky - je�t� nem� vyk�z�no, bude to cca 2 MD (dnes hotov�)
*JCO
 -CR-2415 repliky - pot�ebuje konzultaci s AMO ~2MH
 -CR-2605 iOS - po��t� 1MD
*SHO
 -CR-2600_1 Konfirmace hl�ka
 -CR-2570 Trace - pr� moc slo�it� (odhad DEV 2,5 MD) - a� ud�l� s MBA za z�dy
*JJE
 -CR-2558 Mobiln� platby
.


Revize 8.11.2016 ----------------------------------------------------

IFAT!
- CR-2557, CR-2558 - ov��it, ��m je blokov�no - n�zev replik - kdy�tak naspat okam�it� do YT
  - �dajn� �e�� AMO - ale po��ouchnout je�t� dnes
- CR-2573 - prov��it ty blokery

nov� CR:
- CR-2622 replika
- CR-2673 Trace - bude asi dnes za�azeno - a� si MBA �ekne, co je pot�eba za design
- patch! CR-2692 �im�nek del�� napis - po�leme cenu
- patch! CR-2688 MZA k�eft s dokumenty - cena posl�na, bude rozhodnuto zejtra

Ostatn�:
- RAM pro SHO - MZA zkus�
- Prostupy XTC->XUC - �e�� se

Sch�zky napl�novat na p�tek:
- JCO FB, AMO FB, PMI APCIC
- pozor v p�tek 13-14 je PKR pokec
- ve�er p�ed sch�zkou poslat info


TSPI:

Automaty
- nejsou nyn� udr�ovan�
- je pot�eba revidovat stav, zkontrolovat v�sledky a alespo� 1x za release �e�it, co je tam za probl�m
  - JHE vytipuje �lov�ka a zad� mu to pro tento release
  - utilizovat na CT
- krom� toho jednor�zov�:
  - jak to ud�lat, aby testy d�valy relevantn� v�sledky �ast�ji - prost� l�pe vyu��t potenci�l t�ch test�:
    - nap�. vytipovat skupinu test�, kter� by mohly b�et st�le
	- zbytek nap�. posledn�ch p�r t�dn� p�ed releasem

Aktu�ln� TSPI pro kohokoliv
- SPI-215 Vy�i�t�n� _grants.sql  -- AMO
- SPI-217 Nekonzistence DB schematu  -- AMO
- SPI - unit testy pro DB

Aktu�ln� TSPI pro specialisty
- SPI-206 Zdroj ��seln�k� v Demu  -- RMU
- SPI-074 Hudson findbugs/checkstyle jako sou��st buildu  -- VSP
- SPI-209 Upsource - pilot  -- VSV
- SPI-214 Aktualizace postupu p��pravy v�vojov�ho prost�ed� CIC  -- VSV
- SPI-211 MBA Source Code Policy  -- VSV ve spolupr�ci s LSU
- SPI-212 Trackov�n� zm�n v EA  -- TJE
- SPI-203 Pou�it� lok�ln�ch artefakt� pro build integra�n�ch test�  -- TJE
- SPI-216 Infra monitor web dashboard  -- TSE, VSV

MZA
+ PKI override - uvid�me na ja�e 2017
+ Inovace IB - mo�n� ty tabulky - proj�t s MBA

+ metriky - vyu��vat nov� n�stroje - nap�. Sonar
 - ��dky k�du - st�vaj�c� postup d�v� nesmysln� ��sla
 - t�eba i po�ty defekty
 - a neum� t�eba i analyzovat ta data? - po�ty defekt� na po�et ��dek apod.

+ unit testy - ud�lat checklist na revizi a p�idat si tam, �e je pot�eba d�lat unit testy i tam, kde se jen n�co m�n�
+ p�i oprav� HP napsat n�kam, jestli je mo�n� na to napsat unit test - error_type K21

+ HP metriky
 - jak zjistit po�et HP, kter� p�es n�s nej p�e�ly a kter� byly opravdu chyby u n�s:
   - Last solving system: not "" and not "CIC" - n�kde to jsou ale rejecty a to u� nen� tak jednoduch� filtrovat
.


14.11.2016 ----------------------------------------------------

SHO -> m� trace CR-2570 - jen hl�dat, �e se v�bec n�co d�je - jak utilizuje?
DPE -> m� CR-2600_4 rozprac hrom platba - d�l� s RMU, orienta�n� bude a� v p���t�m t�dnu
RMU -> pom�h� DPE a p��padn� m��e �e�it SPI-206 Zdroj ��seln�k� v Demu

AMO -> nyn� je�t� n�jak� revize a pak SPI - t�eba ty DB unit testy

JCO -> SPI-216 form�tov�n� pubmonitoru + p��padn� n�jak� jeho p�ipom�nky, pak zbyl� CR/HP
JJE -> r�no zbytky z reviz�? pak zbyl� CR/HP

LSU -> st�le m� to CR-2600_05 - stav?
 -> u� je vy�e�eno

TODO:
 - zkontrolovat utilizaci SHO a DPE
 - iniciovat sch�zku MZA, MSI, RMU k SPI-206 Zdroj ��seln�k� v Demu
 - p�idat do pl�nu CR-2688



Revize 15.11.2016 ----------------------------------------------------

nov� CR do TRACE
- CR-2673
- CR-2683
- ?? mo�n� bude za�azeno CR-2691

CR-2573 - do�e�it v�ci z revize
CR-2570 - zat�m v�e vyk�z�no na dokov�n� - SHO m� �vazek 0,5                    <----------
CR-2415 - je tam n�co z revize - pot�eba ov��it, jestli lze bez rizika testovat <----------

CR-2682
CR-2677
CR-2673
+ CR-2683

-> p�edat n�komu tyto nov� CR na design - AMO, RMU, VSV, LSU

CR-2680 - Aktivita sledov�n� transakc� p�ed�van�ch na BE
- SP asi nad logtrn
- stihneme to do ZR? jestli ano, tak by to bylo placeno z APCIC
- m��eme �ekat, �e to p�ijde
- celkov� asi 11 MD


17.11.2016 ---------------------------------------------------------------

Probrat s JHE optimalizaci tabulky p�ehledu CR v souvislosti efektivn�ho p�echodu FUN mezi jednotliv�mi f�zemi:
1. CR je k nacen�n�
2. CR je nacen�no a �ek� na schv�len�
3. CR je za�azeno (typicky nacen�no a schv�leno) a �ek� na DES
4. DES hotov� a �ek� na DEV
5. DEV hotov� a �ek� na DEV_REV -> je mo�n� za��t testy, pokud nen� co jin�ho d�lat a pokud se neo�ek�v� z�sadn� nedostatky v DEV
6. DEV_REV hotov� a p�ipom�nkami, �ek� na zapracov�n� p�ipom�nek - vr�ceno do DEV
7. DEV_REV hotov� bez (dal��ch) p�ipom�nek -> je mo�n� efektivn� testovat
8. CR k testu, �ek� se na data -> blokov�no kv�li dat�m
9. CR k testu, data jsou dostupn� -> je mo�n� efektivn� testovat

Mail k pou��v�n� YT a Upsource
TO: cic_dev
SUBJ: CIC DEV newsletter v38/1 - pilotn� provoz YT a Upsource

Moji mil� v�voj��i,
dovoluji si sepsat p�r informac� systematicky, abych dal plo�n� z�kladn� informovanosti za dost.

1. CIC_DEV YT:
Asi jste si ji� v�imli, �e v tomto release jsem zavedl op�t YouTracky pro evidenci v�voje jednotliv�ch funk�nost� (typicky per CR, p��padn� dle kapitoly - nyn� jen u CR-2600).
M� motivace k tomuto kroku je takov�:
- m�t mo�nost zapisovat specifika n�kter�ch rozhodnut� ve v�voji (n�kdy moc slo�it� popsat do javadocu)
- evidovat stav v�voje - nap�. v situaci, kdy nen� mo�n� nyn� pokra�ovat, nebo se v�voj p�ed�v� a jin�ho v�voj��e
- evidovat revize k�du - bu� p��mo, nebo odkazem do Upsource (viz d�le)
- ka�d� m� jasn� zadanou svou pr�ci (YT je p�i�azen na n�j, p�ijde mu email)
Ch�pu, �e se jedn� o dal�� administrativu, nicm�n� v���m, �e ve spojen� s p�ehledovou tabulkou funk�nost� dan�ho release (gdoc https://docs.google.com/spreadsheets/d/18xzUo4G0kX1V_Hd-QG8vGAVGXKoV5e1aNoVh0PaspEc/edit#gid=0) p�in�� lep�� p�ehled o v�voji pro v�echny.
Jsem ale samoz�ejm� otev�en va�im p�ipom�nk�m, n�pad�m a post�eh�m i v tomto p��pad�.

2. Evidence reviz� k�du v Upsource



21.11.2016 -----------------------------------------------------------------

JJO:
- CR-2622 - dod�l�v�
- CR-2600_3 - drobn� zm�na hl�ky

JCO:
- nemoc/dovolen�
- CR-2559 SEPA - do�e�� AMO

DPE
- p�ipom�nky z revize
- CR-2600_04

SHO
- CR-2673 trace

RMU
- CR-2600_04
- HP

LSU
- revize
- prod defekt od Milo�e

AMO
- CR-2559 SEPA - do�e�� po JCO
- revize
-> zad�no SPI-unit testy pro DB


Revize 22.11.2016 ----------------------------------------------------

CR-2691 - Trace CBL - nov� CR k v�voji, hodit do pl�nu
CR-2698 - Trace PNT

LDA - je�t� n�jak� designy?
stav CR-2600_4 ? urychlit v�voj co nejv�c
 - dle odhadu RMU bude do konce t�dne ~ 2,5 MD

JCO - p�ehodit na testy po dohod� s JHE
 - v nejbli���ch dnech by �e�il defekty na MBA
JJE - p�ehodit na testy po dohod� s JHE
 - v�voj CR-2691
DPE - p�ehodit na testy po dohod� s JHE
SHO - p�ehodit na testy po dohod� s JHE

CPS m� na INT PR verzi, ZR nest�haj� a nebudou nasazovat
-> bude asi mezirelease

IFAT
- BESC nebude v ZR - CPS to ned�v�
- Migrace - od n�s to chod�, ale u nich to zat�m nikdo nezpracuje - za n�s OK, jen �ek�me na OK od nich
- Trace pilot - zat�m n�s neum�j� provolat
- TFI asi zase nebude - asi se �pln� zru��

5.,6.12. je pl�n na zah�jen� CT

SHO, DPE - ov��it pokroky v DEV

Zp�tn� vazby - MZA: MVO, LSU, JJE - do p�lky p���t�ho tejdne sepsat n�jak� body


Provoz
Pavla
- Ivana
- Martina
- Vlasta Snopek
- P�tek, Vesel�

Marek Vesel�
- Iveta - scope
- JJO
- David Varga
- J�im
...


23.11.2016 -----------------------------------------------------------------

Trace CR:
CR-2570 - TraceIII - v�t�ina v�c� jen test, kap. 3 hotovo a �ek�me na repliku s ��seln�kem
CR-2683 - �ek�me na anal�zu probl�mu na produkci
CR-2698 - printNet - �ek�me jak bude vypadat ten ��seln�k, jinak je p�edp�ipraven� - cca do 0,5 dne

24.11.2016 -----------------------------------------------------------------

JJE - CR-2691 hl�s� te� kolem 90% hotovo - asi bude m�t zejtra k revizi -> AMO
 -> d�l na testy
AMO - CR-2680 bude m�t nejpozd�ji b�hem zejtra hotovo -> revize RMU nebo VSV
 -> revize CR-2691, pak CR-2600_04, pak asi SPI
RMU - CR-2600_04
 -> d�l defekty, p��padn� SPI ?
DPE - CR-2600_04 - m�lo by b�t do konce tejdne hotovo - k reivzi -> quick VSV
 -> d�l na testy
 -> p��padn� pokud by byly chyby na Android, tak si m��e n�jak� vz�t
SHO - CR-2673 - bude m�t dnes nebo zejtra -> revize MBA
 -> d�l volno, resp. testy
LSU - mobilka mimo, asi nen� te� chv�li t�eba v�bec �e�it
JCO - �e�� chyby z mobilky, zat�m vyt��en�

Pot�ebujeme tedy SPI pro RMU a AMO
- SPI-206 Zdroj ��seln�k� v Demu  -- je pot�eba svolat sch�zku: RMU, VSV, MZA, MSI
- SPI-216 pubmonitor -- funkce u� celkem zafixovan�, bude pot�eba vylep�it trochu UX (aktu�ln� �e�� JCO ve voln�m �ase)
 - sepsat popis k MW -> to m��e b�t extra TSPI
- SPI-074 SonarQube -- zjistit stav !!!
* automat na KRLI - AMO m� z�jem �e�it

28.11.2016 -----------------------------------------------------------------

Trace CR:
st�le stejn� - RDS nepochopila zad�n� - stav se mo�n� zm�n� do 2 dn� (st�eda, �tvrtek)
CR-2570 - MBA: TraceIII - v�t�ina v�c� jen test, kap. 3 hotovo a �ek�me na repliku s ��seln�kem
CR-2698 - MBA: printNet - �ek�me jak bude vypadat ten ��seln�k, jinak je p�edp�ipraven� - cca do 0,5 dne
CR-2683 - MBA: �ek�me na anal�zu probl�mu na produkci
 - st�le nen� jasn�, co je za probl�m na t� produkci
 - Michal zat�m zkusil obej�t knihovnu a napsat si basic s�m, nyn� �e�� s Elasticem, jestli je takto OK

CR-2673 - SHO: validace
 - chyb� je�t� validace dle ��seln�ku

CR-2600_04 - RMU/DPE: hotovo
CR-2691 - JJE: p�ed�v�n� chyb�j�c�ch dat
 - mo�n� dnes hotovo

Dal�� pr�ce
RMU: HP/SPI
DPE: testy - p��padn� m��e doplnit tu tabulku MW zpr�v (SPI-216 pubmonitor)
AMO: reivze
JCO: HP MBA
VSV: PM, review CR-2600_04, SPI-074 Sonar (metriky), SPI-214 aktualizace DEV ENV, APCIC
LSU: HP, MBA mimo
SHO: dod�l�v� v�voj, pak testy
JJE: dod�l�v� v�voj, pak m� n�jak� HP


29.11.2016 -----------------------------------------------------------------

Nov� �lov�k z matfyz - �tvrtek cel� den, �ter� a p�tek p�lden (Martin Mach)
 - d�me na testy, ale p�ipravovat si v�voj - v �noru m� j�t na WBL
HP na PKI
 - Alex napsat, co v�, �e se tam d�je - kdo a kdy zapisuje a �te t� tabulky - co se d�je p�i p�ihl�en� a p�i tom zkr�cen� (prost� dle sc�n��e v tom HP)

Revize 30.11.2016 ----------------------------------------------------

Trace:
CR-2570 - MBA: TraceIII
CR-2698 - MBA: ��seln�k v�era ve�er p�i�el, odpoledne by to mohlo b�t testovateln�
CR-2683 - MBA: dvoj� zas�l�n� do ELASTIC
 - JEST knihovna - patch posl�n, byl akceptov�n, ale nen� jasn�, kdy vydaj� novou verzi
 - 7. nebo 8.12.2016 se bude povy�ovat minor verze ELASTIC

CR-2573 - AMO: TraceIII - LKP_TRACE_MAPPING -> bude �e�it MZA, pro� z toho nen� dal�� CR
CR-2673 - SHO: validace - dneska, zejtra dorazit se mnou!!!
CR-2600_04 - RMU/DPE: je�t� dnes se �e�� p�ipom�nky z revize - dnes odpoledne nebo zejtra bude k testu

Nov� CR:
CR droben� dat v ELASTIC
 - MZA chce a� do JR a snad to tak vyjde

CPS mezirelease:
 - CPS bude ZR dod�vat v �noru
 - za n�s se t�k� CR-2538 BESC a ��sti CT
 - d�l n�s to bude brzdit s v�vojem - budeme muset dr�et v38 na ST2 del�� dobu

JCO - ��sten� zkus�me poslat LME na anal�zu a nakonec tla�it na jin� projekt


TODO: zeptat se SHO, jestli by n�m necht�l je�t� pomoct s testama - nav��ili bychom �vazek na prosinec
TODO: pomoc dneska SHO s dot�hnut�m CR-2673!

TODO: potom se vrhnout i na defekty z mobilky - jakmile bude �as
TODO: zejtra v�novat se Martinu Machovi - z pohledu p��pravy PC pro v�voj

metriky - LoC za release, HP za release


VLE - zejtra p�ijde - bude 2 dny - pr�ce:
1. Trace - MBA m� asi 1 defekt, kter� by si mohl vz�t
2. Trace - mo�n� by mohl ud�lat revizi �i obecn� dot�hnout to CR-2673 server side validace po SHO (z�le�� dle stavu v�voje)
3. defekty MBA - jestli je�t� n�co bude na Android
4. defekty ostatn� - m�me n�co v integrac�ch


Revize 5.12.2016 -----------------------------------------------------------

Trace:
CR-2570 - MBA: TraceIII
 - kap 5, ods 6 - kolek�n� data -> nov� CR-2716 2,5 MB nacen�no - MBA ji� na tom za�al d�lat
 - zat�m 8 hodin + je�t� n�co bude
 - kap 7 - support pro nasazen� nov� knihovny - jen podpora test� (~2MD)
CR-2673 - SHO: validace - MBA nyn� reviduje a n�kter� v�ci asi nech� dod�lat/p�ed�lat VLE
CR-2683 - MBA: dvoj� zas�l�n� do ELASTIC
 - JEST knihovna - patch posl�n, byl akceptov�n, ale nen� jasn�, kdy vydaj� novou verzi
 - 7. nebo 8.12.2016 se bude povy�ovat minor verze ELASTIC
  - jestli bude fungovat, tak jsme za v�voj hotov� - je pot�eba otestovat plo�n�ji (po pov��en� ELASTIC)
CR-2698 - MBA: hotovo


CR-2573 - AMO: TraceIII - LKP_TRACE_MAPPING -> bude �e�it MZA, pro� z toho nen� dal�� CR
 - RMU poslal mailem tabulku, ke kter� se Zbyn�k do zejtra vyj�d��
 - po��t�me tak 0,5 MD, pokud se to schv�l� takto


HPv38:
+ 78384 - S24_DB_statistiky segment� - vy�e�il TJE
dal�� po�le je�t� dnes Vl��a


Opravy chyb: RMU a VSV a ��ste�n� LSU a JCO (MBA/iOS)
V�ichni ostatn� testuj�
- p�ehodit defekty z SHO a JJE

EN textace jsou ji� v EA -> dod�lat, kde chyb�lo na v�voji


JR'17
 - PKI nebudou
 - velikost ~160 - asi jako ZR'17
 - velk� CR: watchdogy, MBA UX,..


SHO, DPE - ud�lat s RMU hodnocen� - tento t�den
- ud�lat sch�zku a p�izvat MZA


v�voj JR'17
- SHO
? DPE
+ matu� (asi od Nov�ho roku - prost� po testech)
? Martin Mach
- RMU - dovolen�
+ VLE

new
? V�t R��ek - asi jen test
? leden - taky test
? �nor - taky test
